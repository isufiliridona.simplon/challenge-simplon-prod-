<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert([
            'name' => 'Student1',
            'surname' => 'Simplon',
            'email' => Str::random(10).'@gmail.com',
            'url' => 'http://www.test.com/ ' . Str::random(5),
            'promo_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);


        DB::table('students')->insert([
            'name' => 'Student2',
            'surname' => 'Simplon',
            'email' => Str::random(10).'@gmail.com',
            'url' => 'http://www.test.com/ ' . Str::random(5),
            'promo_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);


        DB::table('students')->insert([
            'name' => 'Super Student1',
            'surname' => 'Simplon',
            'email' => Str::random(5).'@gmail.com',
            'url' => 'http://www.test.com/ ' . Str::random(5),
            'promo_id' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);


        DB::table('students')->insert([
            'name' => 'Super Student2',
            'surname' => 'Simplon',
            'email' => Str::random(5).'@gmail.com',
            'url' => 'http://www.test.com/ ' . Str::random(5),
            'promo_id' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);


        DB::table('students')->insert([
            'name' => 'Super Student3',
            'surname' => 'Simplon',
            'email' => Str::random(5).'@gmail.com',
            'url' => 'http://www.test.com/ ' . Str::random(5),
            'promo_id' => 3,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}

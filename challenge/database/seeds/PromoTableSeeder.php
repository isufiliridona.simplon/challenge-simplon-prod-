<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PromoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('promo')->insert([
            'title' => 'promo ',
            'startDate' => Carbon::create(2019, 6, 10),
            'endDate' => Carbon::create(2019, 6, 15),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('promo')->insert([
            'title' => 'Superpromo ',
            'startDate' => Carbon::create(2018, 6, 10),
            'endDate' => Carbon::create(2018, 6, 15),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
